Command line instructions

Git global setup
git config --global user.name "RIOU Kévin"
git config --global user.email "riou.kevin.versioning@gmail.com"

Create a new repository
git clone https://gitlab.com/lpweb-groupe3/feuillederoute.git
cd feuillederoute
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/lpweb-groupe3/feuillederoute.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/lpweb-groupe3/feuillederoute.git
git push -u origin --all
git push -u origin --tags

AUTHORS
RIOU Kevin
DE SOUSA Corentin
DELAPIERRE Solène
DAVID Benoit
